import { Injectable } from '@angular/core';
import * as bcrypt from 'bcryptjs';

const salt = bcrypt.genSaltSync(10);

@Injectable({
  providedIn: 'root'
})

export class BcryptServiceService {

  constructor() { }
    //encrypt password
 setBcryptPassword(value: string){
  return bcrypt.hashSync(value, salt);
 }

 //return true:false
 getComparePassword(pws: string,pwsDB: string){
  return bcrypt.compare(pws,pwsDB);
 }

}
