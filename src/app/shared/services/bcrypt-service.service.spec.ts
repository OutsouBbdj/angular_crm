import { TestBed } from '@angular/core/testing';

import { BcryptServiceService } from './bcrypt-service.service';

describe('BcryptServiceService', () => {
  let service: BcryptServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BcryptServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
