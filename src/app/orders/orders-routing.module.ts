import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageAddOrderComponent } from './pages/page-add-order/page-add-order.component';
import { PageEditOrderComponent } from './pages/page-edit-order/page-edit-order.component';
import { PageListOrderComponent } from './pages/page-list-order/page-list-order.component';
import {PageAddCustomerComponent} from "../customers/pages/page-add-customer/page-add-customer.component";
import {DescrOrderComponent} from "./pages/descr-order/descr-order.component";

const routes: Routes = [
  {path:'list', component: PageListOrderComponent},
  {path:'add', component: PageAddOrderComponent},
  {path:'description/:id', component: DescrOrderComponent},
  {path:'edit/:id', component: PageEditOrderComponent},
  {path:'', redirectTo: 'list', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
