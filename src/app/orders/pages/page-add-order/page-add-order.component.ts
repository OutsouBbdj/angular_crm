import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Customer } from 'src/app/core/models/customer';
import { Order } from 'src/app/core/models/order';
import { Product } from 'src/app/core/models/product';
import { CustomerService } from 'src/app/customers/services/customer.service';
import { ProductService } from 'src/app/products/services/product.service';
import { OrdersService } from '../../orderService/orders.services';
import { PageEditOrderComponent } from '../page-edit-order/page-edit-order.component';

@Component({
  selector: 'app-page-add-order',
  templateUrl: './page-add-order.component.html',
  styleUrls: ['./page-add-order.component.scss'],
})
export class PageAddOrderComponent implements OnInit {
  public success = false;
  public failure = false;
  private souscription: Subscription | null = null;

  public ClientId!: number;
  public ProductId!: number;

  public clients: Customer[] | null = null;
  public produits: Product[] | null = null;

  public formulaireOrder!: FormGroup;

  public customerDtolId!: Customer;
  public productDtolId!: Product;

  constructor(
    private formBuilder: FormBuilder,
    private OrdersService: OrdersService,
    private customersService: CustomerService,
    private productsService: ProductService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }





  ngOnInit(): void {

// Ici je recupére les données correspondants aux clients ainsi qu'aux produits 
// pour pouvoir implementer les champs du choix du client et du produit qui sont .

    this.customersService.getAllCustomers().subscribe((data) => {
      this.clients = data;
    });

    this.productsService.getAllProducts().subscribe((datap) => {
      this.produits = datap;
    });

//Ici je positionne des validateurs de champs (require = obligatoire) ou (pattern pour 
// personnalisé les caracteres du champs de saisie)

    this.formulaireOrder = this.formBuilder.group({

      customerId: ['',[Validators.required]],

      productId: ['',[Validators.required]],

      adrEt: [null,[Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],

      numberOfDays: [null,[Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],

      tva: [null, [Validators.required]],

      status: [null, [Validators.required]],

      type: [null, [Validators.required]],

      notes: ['', [Validators.maxLength(254)]],

    });


  }

  ngOnDestroy(): void {
    // Destruction de l'observateur
    if (this.souscription) {
      this.souscription.unsubscribe();
    }
  }


  public orderAdd!: Order;

  // En appuyant sur le bouton "ajouter" cette fonction permet de valider  
  // le formulaire et declancher une requete Post au serveur.
  addCreatedOrder() {
    this.productDtolId = new Product({});
    this.customerDtolId = new Customer({});
    this.productDtolId.id = this.formulaireOrder.value.productId;
    this.customerDtolId.id = this.formulaireOrder.value.customerId;
    this.orderAdd = this.formulaireOrder.value;
    this.orderAdd.customerDto = this.customerDtolId;
    this.orderAdd.productDto = this.productDtolId;
    this.OrdersService.addOrder(this.orderAdd).subscribe({
      next: () => {
        this.success = true;
        this.goBackToList();
      },
      error: () => {
        this.failure = true;
      },
    });
    this.customerDtolId.id = this.formulaireOrder.value.customerId;
    this.productDtolId.id = this.formulaireOrder.value.productId;
  }

  goBackToList() {
    this.router.navigate(['/orders/list'], { relativeTo: this.activatedRoute });
  }
}
