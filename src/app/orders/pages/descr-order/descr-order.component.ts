import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {Customer} from 'src/app/core/models/customer';
import {Order} from 'src/app/core/models/order';
import {OrdersService} from '../../orderService/orders.services';
import {CustomerService} from "../../../customers/services/customer.service";


@Component({
  selector: 'app-descr-order',
  templateUrl: './descr-order.component.html',
  styleUrls: ['./descr-order.component.scss']
})
export class DescrOrderComponent implements OnInit {


  public order: Order = {} as Order;
  // info commande
  public orderid: number = 0;
  page = 1;
  pageSize = 5;
  public total = 0;

  private souscriptionGetVersion: Subscription | null = null;

  constructor(
    private activatedRoute: ActivatedRoute,
    private orderService: OrdersService,
    private customerService: CustomerService) {

  }

  ngOnInit(): void {
    // on récupère la dernière partie de l'URL pour obtenir le order Id
    this.activatedRoute.url.subscribe(
      url => {
        this.orderid = Number(url[url.length - 1]);

        // On recherche l'order
        this.orderService.getOrderById(this.orderid).subscribe(
          (order: Order) => {
            this.order = order;
          }
        );
      }
    );
  }


  ngOnDestroy(): void {
    // Destruction de l'observateur
    if (this.souscriptionGetVersion) {
      this.souscriptionGetVersion.unsubscribe();
    }
  }
}



