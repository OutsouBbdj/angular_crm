import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DescrOrderComponent } from './descr-order.component';

describe('DescrOrderComponent', () => {
  let component: DescrOrderComponent;
  let fixture: ComponentFixture<DescrOrderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DescrOrderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DescrOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
