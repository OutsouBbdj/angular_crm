import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent implements OnInit {

  ngOnInit(): void {
  }

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  goToLogin() {
    this.router.navigate(['../../home'], { relativeTo: this.activatedRoute });
  }

}
