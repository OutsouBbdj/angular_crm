import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subscription, tap } from 'rxjs';
import { AuthService } from 'src/app/auth/service/auth.service';
import { Order } from 'src/app/core/models/order';
import { Product } from 'src/app/core/models/product';
import { Role } from 'src/app/core/models/roles';
import { OrdersService } from 'src/app/orders/orderService/orders.services';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-page-list-product',
  templateUrl: './page-list-product.component.html',
  styleUrls: ['./page-list-product.component.scss']
})
export class PageListProductComponent implements OnInit {

  public success = false;
  public failure = false;
  public products$!: Observable<Product[]>;
  public productName = '';
  public total = 0;
  public deleteProductID: number = 0;
  private souscription: Subscription | null = null;
  page = 1;
  pageSize = 10;
  private produc !: Product;
  private tabloDeCommande !: Order[];
  public messageDErreurCommandeEnCours=" Suppression impossible. Le produit est affecté a des commandes."
  public isAdmin=false;
  public errorMessage="";

  constructor(
    private productService: ProductService,
    private modalService: NgbModal,
    private orderService: OrdersService,
    private authService:AuthService
  ) { }

  ngOnInit(): void {
    this.isAdmin=Role.Admin===this.authService.getUserLogGrants();
    this.fetchClients();
    
  }

  ngOnDestroy(): void {
    // Destruction de l'observateur
    if (this.souscription) {
      this.souscription.unsubscribe();
    }
  }

  fetchClients() {
    this.products$ = this.productService.getAllProducts().pipe(
      // map(clients => [clients[0]])
      tap(products => this.total = products.length)
      
    );
  }

  deleteClient(productId: number, productName: string) {
    this.productName = productName;
    this.productService.deleteProduct(productId)
    .subscribe(
      {
        error: (error) => {
          this.failure = true;
          this.errorMessage=error.error.message
        },
        complete: () => {
          this.fetchClients();
          this.success = true;
        }
      }
    );

  }

  confirmDeletion(content: any, productId: number, productName: string) {
    this.productName = productName;
    this.modalService.open(content,
      {ariaLabelledBy: 'modal-basic-title'})
      .result.then((result) => {
      if (result) {
        this.deleteClient(productId, productName);

      }
    }, (reason) => {

    });
  }



}
