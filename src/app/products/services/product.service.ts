import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/auth/service/auth.service';
import { Product } from 'src/app/core/models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private url = '/api';

  constructor(private httpClient: HttpClient,
              private authService : AuthService) { }

  
  getAllProducts(): Observable<Product[]> {
    return this.httpClient.get<Product[]>(`${this.url}/products`,{headers: this.authService.getHeaders()});
  }

  getProductById(productId: number): Observable<Product> {
    return this.httpClient.get<Product>(`${this.url}/products/${productId}`, {headers: this.authService.getHeaders()});
  }

  addProduct(product: Product): Observable<void> {
    return this.httpClient.post<void>(`${this.url}/products`, product, {headers: this.authService.getHeaders()});
  }

  deleteProduct(productId: number): Observable<void> {
    return this.httpClient.delete<void>(`${this.url}/products/${productId}`, {headers: this.authService.getHeaders()});
  }

  updateProduct(product: Product): Observable<void> {
    return this.httpClient.put<void>(`${this.url}/products/${product.id}`, product, {headers: this.authService.getHeaders()});
  }






}
