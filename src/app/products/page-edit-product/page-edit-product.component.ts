import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Product } from 'src/app/core/models/product';
import { VersionService } from 'src/app/shared/services/version.service';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-page-edit-product',
  templateUrl: './page-edit-product.component.html',
  styleUrls: ['./page-edit-product.component.scss']
})
export class PageEditProductComponent implements OnInit, OnDestroy {

  public productId: number = 0;
  public product: Product = {} as Product;

  public productName : string = '';
  public productIsAvailable: string = '';
  public souscriptionGetVersion: Subscription | null = null;
  public Available=false;
  public success = false;
  public failure = false;
  public formulaire!: FormGroup;


  constructor(
    private activatedRoute: ActivatedRoute,
      private productService: ProductService,
      private router: Router,
      private versionService: VersionService,
      private formBuilder:  FormBuilder
  ) { }

  initForm(product: Product): void {
    this.formulaire = this.formBuilder.group({
      id:[''],
      productname:['', [Validators.required, Validators.minLength(2), Validators.maxLength(100),Validators.pattern("^[A-Z]{2,100}$")]],
      availablity:[ false , [Validators.required]],
      description: ['', [Validators.maxLength(255)]]
  })
}

  ngOnInit(): void {
    this.activatedRoute.url.subscribe(
      (url: string | any[]) => {
        this.productId = Number(url[url.length -1])
    // On recherche le Client
        this.productService.getProductById(this.productId).subscribe(
          (product: Product) => {
            this.product = product;
            this.productName = this.product.productname;
            this.Available =this.product.availablity;
            this.initForm(product);
          } // permet d'afficher le Titre de la page Edit avec le nom du Customer
        );
      }
    );
  }

  ngOnDestroy(): void {
    if (this.souscriptionGetVersion) {
      this.souscriptionGetVersion.unsubscribe();
    }
  }

  goBackToList() {
    // navigateByUrl n'utilise que des chemins absolus
    // this.router.navigateByUrl('/customers/list');
    this.router.navigate( ['../../listpro'], {relativeTo: this.activatedRoute});
  }

  updateProduct() {
    this.product.availablity=this.formulaire.value.availablity;
    this.productService.updateProduct(this.product).subscribe({
      next: () => { this.success = true; },
      error: () => { this.failure = true; }
    });

  }

}
