import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Product } from 'src/app/core/models/product';
import { ProductService } from '../services/product.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-page-add-product',
  templateUrl: './page-add-product.component.html',
  styleUrls: ['./page-add-product.component.scss']
})
export class PageAddProductComponent implements OnInit {

  public formulaire! : FormGroup;

  public success = false;
  public failure = false;
  public setDisable = false;
  private souscription: Subscription | null = null;

  constructor(
    private http: HttpClient,
    private productService: ProductService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.formulaire = this.formBuilder.group({
      productname:['', [Validators.required
        ,Validators.minLength(2), Validators.maxLength(100)
        ,Validators.pattern("^[A-Z]{2,100}$")]],
      availablity:[true , [Validators.required]],
      description: ['', [Validators.maxLength(255),Validators.required]],
    });
  }

  ngOnDestroy(): void {
    // Destruction de l'observateur
    if (this.souscription) {
      this.souscription.unsubscribe();
    }
  }

  fileName = '';

  onFileSelected(event: any){

    const file:File = event.target.files[0];
    /*this.fichierChoisie = event.target.files[0];
    console.log(event);*/
    if (file) {

      this.fileName = file.name;

      const formData = new FormData();

      formData.append("thumbnail", file);

      const upload$ = this.http.post('C:\\Users\\pc\\Documents\\Developpements\\Angular_projects\\crm-api-angular\\src\\assets', formData);

      upload$.subscribe();
  }

  }

  click(){
    
  }

  /*telechargement(){
    const fd = new FormData();
    fd.append('image', this.fichierChoisie?, this.fichierChoisie?.name);
    this.http.post('', fd).subscribe(res => {
      console.log(res)
    });

  }*/

  ajouter() {
    this.productService.addProduct(
      this.formulaire.value as Product
      ).subscribe({
        next: () => { this.success = true; this.setDisable = true; },
        error:() => { this.failure = true; }
      });
  }

  goBackToList() {
    this.router.navigate(['../listpro'], {relativeTo: this.activatedRoute})
  }


}
