import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription, tap } from 'rxjs';
import { Order } from 'src/app/core/models/order';
import { Product } from 'src/app/core/models/product';
import { OrdersService } from 'src/app/orders/orderService/orders.services';
import { ProductService } from '../services/product.service';


@Component({
  selector: 'app-page-desc-product',
  templateUrl: './page-desc-product.component.html',
  styleUrls: ['./page-desc-product.component.scss']
})
export class PageDescProductComponent implements OnInit {

  public produ !: Product ;
  public orders$!: Observable<Order[]>;
  // info commande
  public orderid: number = 0;
  public product_id: number = 0;
  public total=0;
  page = 1;
  pageSize =5;

  private souscriptionGetVersion: Subscription | null = null;

  constructor(
    private activatedRoute: ActivatedRoute,
    private orderService: OrdersService,
    private productService: ProductService
  ) { }

  ngOnInit(): void {
    

     // on récupère la dernière partie de l'URL pour obtenir le customer Id
   this.activatedRoute.url.subscribe(
    url => {
       this.product_id = Number(url[url.length - 1]);

      // On recherche le customer
      this.productService.getProductById(this.product_id).subscribe(
        (product)=> {
          this.produ = product

        }
      )
     }
      );
       this.fetchOrders();
  }

  fetchOrders() {
    this.orders$ = this.orderService.getOrderByproductId(this.product_id).pipe(
    tap( orders => this.total = orders.length)
      );
    this.orders$.forEach(element => {
         });
    }

    ngOnDestroy(): void {
      // Destruction de l'observateur
      if (this.souscriptionGetVersion) {
        this.souscriptionGetVersion.unsubscribe();
      }
    }

}
