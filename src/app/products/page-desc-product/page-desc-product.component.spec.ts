import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageDescProductComponent } from './page-desc-product.component';

describe('PageDescProductComponent', () => {
  let component: PageDescProductComponent;
  let fixture: ComponentFixture<PageDescProductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageDescProductComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageDescProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
