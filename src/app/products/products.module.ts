import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { PageAddProductComponent } from './page-add-product/page-add-product.component';
import { PageEditProductComponent } from './page-edit-product/page-edit-product.component';
import { PageListProductComponent } from './page-list-product/page-list-product.component';
import { PageDescProductComponent } from './page-desc-product/page-desc-product.component';
import { IconsModule } from '../icons/icons.module';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    PageAddProductComponent,
    PageEditProductComponent,
    PageListProductComponent,
    PageDescProductComponent
  ],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    IconsModule,
    FormsModule,
    NgbModule,
    SharedModule
  ],
  exports: [
    PageAddProductComponent,
    PageEditProductComponent,
    PageListProductComponent,
    PageDescProductComponent
  ]
})
export class ProductsModule { }
