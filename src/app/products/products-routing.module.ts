import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageAddProductComponent } from './page-add-product/page-add-product.component';
import { PageDescProductComponent } from './page-desc-product/page-desc-product.component';
import { PageEditProductComponent } from './page-edit-product/page-edit-product.component';
import { PageListProductComponent } from './page-list-product/page-list-product.component';

const routes: Routes = [
  {path:'listpro', component: PageListProductComponent},
  {path:'addpro', component: PageAddProductComponent},
  {path:'descpro/:id', component: PageDescProductComponent},
  {path:'editpro/:id', component: PageEditProductComponent},
  {path:'', redirectTo: 'listpro', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
