import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from "./service/auth.service";


@Injectable({
  providedIn: 'root'
})
export class IsAdminGuard implements CanActivate {

  constructor(private auth: AuthService,
              private router: Router) {
  }

  canActivate(): boolean {
    const token = this.auth.getToken();
    if (token=="monRoleADMIN") {
      return true;
    } else {
      return false;
    }
  }

}
