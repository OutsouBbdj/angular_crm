import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccueilComponent } from './pages/accueil.component';
import { AccueilRoutingModule } from './accueil-routing.module';
import { TemplatesModule } from '../templates/templates.module';



@NgModule({
  declarations: [
    AccueilComponent
  ],
  imports: [
    CommonModule,
    AccueilRoutingModule,
    TemplatesModule
  ],
  exports: [
    AccueilComponent
  ]
})
export class AccueilModule { }
