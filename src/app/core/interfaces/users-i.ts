export interface UsersI {
    id: number;
    username: string;
    password: string;
    mail: string;
    grants: string;
  }