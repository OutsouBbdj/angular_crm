export interface ProductsI {
    id: number;
    productname: string;
    availablity: boolean;
    description: string;
  }