export interface CustomersI {
  id: number;
  lastname: string;
  firstname: string;
  company: string;
  mail: string;
  phone: string;
  mobile: string;
  active: boolean;
  notes: string;
}
