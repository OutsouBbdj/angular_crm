import { ProductsI } from "../interfaces/products-i";

export class Product implements ProductsI {
    id: number = 0;
    productname: string = '';
    availablity: boolean = false;
    description: string= '';

  constructor(partialProduct: Partial<Product>) {
    if (partialProduct) {
      Object.assign(this, partialProduct);
    }
  }



}