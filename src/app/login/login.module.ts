import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login.routing.module';
import { RouterModule } from '@angular/router';
import {LoginComponent} from "./pages/login.component";
import {CoreModule} from "../core/core.module";
import {NgbToastModule} from "@ng-bootstrap/ng-bootstrap";


@NgModule({
  declarations: [LoginComponent],
    imports: [
        CommonModule,
        LoginRoutingModule,
        RouterModule,
        CoreModule,
        NgbToastModule,
    ],
  exports :[LoginComponent]
})
export class LoginModule { }
