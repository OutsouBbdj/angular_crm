import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../auth/service/auth.service";
import { User } from 'src/app/core/models/user';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public success = false;
  public failure = false;
  public loginForm!: FormGroup;
  private user!: User;
  private souscription: Subscription | null = null;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) {
  }

  ngOnInit(): void {
    this.loginForm =
      this.formBuilder.group({
        username: ['',[Validators.required]],
        password: ['',[Validators.required]]
      });
  }

  ngOnDestroy(): void {
    // Destruction de l'observateur
    if (this.souscription) {
      this.souscription.unsubscribe();
    }
  }

  verifierLogin() {
    this.user = this.loginForm.value;
    this.authService.login(this.user.username,this.user.password);
  }
}
