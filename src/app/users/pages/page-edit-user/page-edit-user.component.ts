import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from 'src/app/core/models/user';
import { BcryptServiceService } from 'src/app/shared/services/bcrypt-service.service';
import { UsersService } from '../../services/users.service/users.service';

@Component({
  selector: 'app-page-edit-user',
  templateUrl: './page-edit-user.component.html',
  styleUrls: ['./page-edit-user.component.scss'],
})
export class PageEditUserComponent implements OnInit {
  public user!: User;
  public userid: number = 0;
  private souscriptionGetVersion: Subscription | null = null;
  public formulaire!: FormGroup;
  public formulairePassword!: FormGroup;
  public success = false;
  public failure = false;
  public setDisable = false;
  public updatePassword = false;
  public userExist = false;
  public errorMessage = '';
  private userId: number = 0;

  constructor(
    private usersService: UsersService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private bcryptServiceService: BcryptServiceService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.url.subscribe((url) => {
      this.userId = Number(url[url.length - 1].toString());

      this.usersService.getUserById(this.userId).subscribe((user) => {
        this.user = user;
      });
    });

    this.formulaire = this.formBuilder.group({
      username: [
        '',
        [
          Validators.minLength(3),
          Validators.maxLength(25),
          Validators.required,
          Validators.pattern('[a-zA-Z0-9]+'),
        ],
      ],
      mail: [
        '',
        [
          Validators.minLength(1),
          Validators.maxLength(255),
          Validators.required,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
        ],
      ],
      grants: ['', [Validators.required]],
    });

    this.formulairePassword = this.formBuilder.group({
      password: [
        '',
        [
          Validators.minLength(5),
          Validators.maxLength(15),
          Validators.required,
          Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$'),
        ],
      ], //this is for the letters (both uppercase and lowercase) and numbers validation
    });
  }

  ngOnDestroy(): void {
    // Destruction de l'observateur
    if (this.souscriptionGetVersion) {
      this.souscriptionGetVersion.unsubscribe();
    }
  }

  update() {
    this.user.username = this.formulaire.value.username;
    this.user.mail = this.formulaire.value.mail;
    this.user.grants = this.formulaire.value.grants;
    this.user.password = 'DEFAULT';
    this.usersService.updateUser(this.user).subscribe({
      next: () => {
        this.success = true;
        this.setDisable = true;
      },
      error: (error) => {
        this.errorMessage = error.error.message;
        this.failure = true;
        this.user.password="";
      },
    });
  }

  updateUserPassword() {
    this.formulairePassword.value.password =
      this.bcryptServiceService.setBcryptPassword(
        this.formulairePassword.value.password
      );

    this.usersService
      .updateUserPassword(this.user.id, this.formulairePassword.value.password)
      .subscribe({
        next: () => {
          this.success = true;
          this.setDisable = true;
        },
        error: (error) => {
          this.errorMessage = error.error.message;
          this.failure = true;
        },
      });
  }

  goToUpdatePassword() {
    this.updatePassword = true;
  }

  goToUpdateUser() {
    this.updatePassword = false;
  }

  goBackToList() {
    this.router.navigate(['../../list'], { relativeTo: this.activatedRoute });
  }
}
