import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subscription, tap } from 'rxjs';
import { AuthService } from 'src/app/auth/service/auth.service';
import { Role } from 'src/app/core/models/roles';
import { User } from 'src/app/core/models/user';
import { UsersService } from '../../services/users.service/users.service';

@Component({
  selector: 'app-page-list-customer',
  templateUrl: './page-list-user.component.html',
  styleUrls: ['./page-list-user.component.scss']
})
export class PageListUserComponent implements OnInit {

  public success=false;
  public failure=false;
  public users$!: Observable<User[]>;
  public username = '';
  public total=0;
  private souscription: Subscription | null = null;
  page = 1;
  pageSize =10;
  public isAdmin=false;
  public errorMessage="";
  constructor(private usersService: UsersService,
  private modalService: NgbModal,
  private authService:AuthService) { }

  ngOnInit(): void {
     this.isAdmin=Role.Admin===this.authService.getUserLogGrants();
     this.fetchUsers();
  }

  ngOnDestroy(): void {
    // Destruction de l'observateur
    if (this.souscription) {
      this.souscription.unsubscribe();
    }
  }

  fetchUsers() {
    this.users$ = this.usersService.getAllUsers().pipe(
      // map(clients => [clients[0]])
      tap( users => this.total = users.length)
      );
  }

  deleteUser(clientId: number) {
    this.usersService.deleteUser(clientId).subscribe(
      { error: (error) => {
        this.failure = true;
        this.errorMessage=error.error.message
      },
        complete: () => {
        this.fetchUsers();
        this.success = true;
      }}
    );

  }

  confirmDeletion(content: any, clientId: number, username: string) {
    this.username = username;
    this.modalService.open(content,
      {ariaLabelledBy: 'modal-basic-title'})
      .result.then((result) => {
        if (result) {
          this.deleteUser(clientId);

        }
    }, (reason) => {

    });
  }

}
