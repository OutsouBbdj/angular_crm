import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DescrUserComponent } from './descr-user.component';

describe('DescrUserComponent', () => {
  let component: DescrUserComponent;
  let fixture: ComponentFixture<DescrUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DescrUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DescrUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
