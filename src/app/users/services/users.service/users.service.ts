import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from "rxjs";
import {User} from "../../../core/models/user";
import {AuthService} from "../../../auth/service/auth.service";


@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private url = '/api';

  constructor(private httpClient: HttpClient,
              private authService: AuthService) {
  }

  getAllUsers(): Observable<User[]> {
    return this.httpClient.get<User[]>(`${this.url}/users`, {headers: this.authService.getHeaders()});
  }

  getUserById(userId: number): Observable<User> {
    return this.httpClient.get<User>(`${this.url}/users/${userId}`, {headers: this.authService.getHeaders()});
  }

  getUserByUsername(username: string): Observable<User> {
    return this.httpClient.get<User>(`${this.url}/users?username=${username}`, {headers: this.authService.getHeaders()});
  }



  addUser(user: User): Observable<void> {
    return this.httpClient.post<void>(`${this.url}/users`, user, {headers: this.authService.getHeaders()});
  }

  deleteUser(userId: number): Observable<void> {
    return this.httpClient.delete<void>(`${this.url}/users/${userId}`, {headers: this.authService.getHeaders()});
  }

  updateUser(user: User): Observable<void> {
    return this.httpClient.put<void>(`${this.url}/users/${user.id}`, user, {headers: this.authService.getHeaders()});
  }

  updateUserPassword(id: number, password: string): Observable<void> {
    return this.httpClient.patch<void>(`${this.url}/users/${id}?password=${password}`, {headers: this.authService.getHeaders()});
  }

}
