import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersRoutingModule } from './users-routing.module';
import { PageEditUserComponent } from './pages/page-edit-user/page-edit-user.component';
import { PageAddUserComponent } from './pages/page-add-user/page-add-user.component';
import { PageListUserComponent } from './pages/page-list-user/page-list-user.component';
import { DescrUserComponent } from './pages/descr-user/descr-user.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { TemplatesModule } from '../templates/templates.module';
import {SharedModule} from "../shared/shared.module";
import { IconsModule } from '../icons/icons.module';



@NgModule({
  declarations: [
    PageEditUserComponent,
    PageAddUserComponent,
    PageListUserComponent,
    DescrUserComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    NgbModule,
    FormsModule,
    TemplatesModule,
    SharedModule,
    ReactiveFormsModule,
    IconsModule
  ],
  exports: [
    PageEditUserComponent,
    PageAddUserComponent,
    PageListUserComponent,
    DescrUserComponent
  ]
})

export class UsersModule { }
