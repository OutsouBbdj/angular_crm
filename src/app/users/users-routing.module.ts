import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageAddUserComponent } from './pages/page-add-user/page-add-user.component';
import { PageEditUserComponent } from './pages/page-edit-user/page-edit-user.component';
import { PageListUserComponent } from './pages/page-list-user/page-list-user.component';
import {DescrUserComponent} from "./pages/descr-user/descr-user.component";
import {AuthGuard} from "../auth/auth.guard";

const routes: Routes = [
  {path:'list', component: PageListUserComponent},
  {path:'add', component: PageAddUserComponent},
  {path:'description/:id', component: DescrUserComponent},
  {path:'edit/:id', component: PageEditUserComponent},
  {path:'', redirectTo: 'list', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
