import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/service/auth.service';

@Component({
  selector: 'app-ui',
  templateUrl: './ui.component.html',
  styleUrls: ['./ui.component.scss']
})
export class UiComponent implements OnInit {

  constructor(private auth: AuthService,) { }

  ngOnInit(): void {
  }

  isConnected(): boolean {
    const token = this.auth.getToken();
    if (token) {
      return true;
    } else {
      return false;
    }

  }
}
