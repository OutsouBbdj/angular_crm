import { Component, OnInit, Output, Input, OnChanges, OnDestroy, SimpleChanges, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-template-page',
  templateUrl: './template-page.component.html',
  styleUrls: ['./template-page.component.scss']
})
export class TemplatePageComponent implements OnInit, OnChanges, OnDestroy {

  @Input()
  public titre: string = 'Mon titre par défaut';

  @Input()
  public visible = true;


  @Output()
  public actionRetour = new EventEmitter<void>();

  ngOnDestroy(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  constructor() {
  }

  ngOnInit(): void {
  }

  retourner() {
    this.actionRetour.emit();
  }

}
