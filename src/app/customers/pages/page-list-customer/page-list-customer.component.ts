import {Component, OnInit} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Observable, Subscription, tap} from 'rxjs';
import {Customer} from 'src/app/core/models/customer';
import {CustomerService} from '../../services/customer.service';
import {OrdersService} from "../../../orders/orderService/orders.services";
import {Order} from "../../../core/models/order";
import { AuthService } from 'src/app/auth/service/auth.service';
import { Role } from 'src/app/core/models/roles';

@Component({
  selector: 'app-page-list-customer',
  templateUrl: './page-list-customer.component.html',
  styleUrls: ['./page-list-customer.component.scss']
})
export class PageListCustomerComponent implements OnInit {
  public success = false;
  public failure = false;
  public customers$!: Observable<Customer[]>;
  public customerLastName = '';
  public customerFirstname = '';
  public total = 0;
  public deleteCustomerID: number = 0;
  private souscription: Subscription | null = null;
  page = 1;
  pageSize = 10;
  private custom !: Customer;
  private tabloDeCommande !: Order[];
  public messageDErreurCommandeEnCours=" Suppression impossible. Le client a des commandes."
  public isAdmin=false;
  public errorMessage="";

  constructor(
    private customerService: CustomerService,
    private modalService: NgbModal,
    private orderService: OrdersService,
    private authService:AuthService
  ) {
  }

  ngOnInit(): void {
    this.isAdmin=Role.Admin===this.authService.getUserLogGrants();
    this.fetchClients();
  }

  ngOnDestroy(): void {
    // Destruction de l'observateur
    if (this.souscription) {
      this.souscription.unsubscribe();
    }
  }

  fetchClients() {
    this.customers$ = this.customerService.getAllCustomers().pipe(
      // map(clients => [clients[0]])
      tap(customers => this.total = customers.length)
    );
  }

  deleteClient(clientId: number, customerLastName: string, customerFirstname: string) {
    this.customerLastName = customerLastName;
    this.customerFirstname = customerFirstname;
    this.customerService.deleteCustomer(clientId).subscribe(
      {
        error: (error) => {
          this.failure = true;
          this.errorMessage=error.error.message
        },
        complete: () => {
          this.fetchClients();
          this.success = true;
        }
      }
    );

  }

  confirmDeletion(content: any, clientId: number, customerLastName: string, customerFirstname: string) {
    this.customerLastName = customerLastName;
    this.customerFirstname=customerFirstname;
    this.modalService.open(content,
      {ariaLabelledBy: 'modal-basic-title'})
      .result.then((result) => {
      if (result) {
        this.deleteClient(clientId, customerLastName, customerFirstname);

      }
    }, (reason) => {

    });
  }
}
