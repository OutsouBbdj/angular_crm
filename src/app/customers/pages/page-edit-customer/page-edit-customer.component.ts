import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Customer } from 'src/app/core/models/customer';
import { VersionService } from 'src/app/shared/services/version.service';
import { CustomerService } from '../../services/customer.service';

@Component({
  selector: 'app-page-edit-customer',
  templateUrl: './page-edit-customer.component.html',
  styleUrls: ['./page-edit-customer.component.scss']
}) export class PageEditCustomerComponent implements OnInit, OnDestroy {

  public customerId: number = 0;
  public customer: Customer = {} as Customer;

  public customerLastname : string = '';
  public firstName: string = '';
  public customerIsActive: string = '';
  public souscriptionGetVersion: Subscription | null = null;
  public active=false;
  public success = false;
  public failure = false;
  public formulaire!: FormGroup;


  constructor(
      private activatedRoute: ActivatedRoute,
      private customerService: CustomerService,
      private router: Router,
      private versionService: VersionService,
      private formBuilder:  FormBuilder)  { }


  initForm(customer: Customer): void {
    this.formulaire = this.formBuilder.group({
        id: [''],
        lastname: ['', [Validators.required,Validators.minLength(2),Validators.maxLength(100),Validators.pattern('^[A-ZÉ ]{2,100}$')]],
        firstName: ['', [Validators.required,Validators.minLength(2),Validators.maxLength(100),Validators.pattern('^[a-zA-ZäâéçÉ ]{2,100}$')]],
        company: ['', [Validators.required,Validators.minLength(2), Validators.maxLength(200)]],
        mail: ['', [Validators.required, Validators.minLength(8),Validators.maxLength(255), Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
        phone: ['', [Validators.required,Validators.minLength(10), Validators.maxLength(10), Validators.pattern(/[0-9]/)]],
        mobile: ['', [Validators.minLength(10), Validators.maxLength(10), Validators.pattern('^(0)[6-7][0-9]{8}$')]],
        active:['',Validators.required],
        notes: ['',Validators.maxLength(255)]
    });
  }

  ngOnInit(): void {
    // on récupère la dernière partie de l'URL pour obtenir le customerId, seule l'activatedRoute le permet !
    this.activatedRoute.url.subscribe(
      (url: string | any[]) => {
        this.customerId = Number(url[url.length -1])
    // On recherche le Client
        this.customerService.getCustomerById(this.customerId).subscribe(
          (customer: Customer) => {
            this.customer = customer;
            this.customerLastname = this.customer.lastname;
            this.firstName = this.customer.firstname;
            this.active=this.customer.active;
            this.initForm(customer);
          } // permet d'afficher le Titre de la page Edit avec le nom du Customer
        );
      }
    );
  }

  ngOnDestroy(): void {
    if (this.souscriptionGetVersion) {
      this.souscriptionGetVersion.unsubscribe();
    }
  }

  goBackToList() {
    // navigateByUrl n'utilise que des chemins absolus
    // this.router.navigateByUrl('/customers/list');
    this.router.navigate( ['../../list'], {relativeTo: this.activatedRoute});
  }

  updateCustomer() {
    this.customer.active=this.formulaire.value.active;
    this.customerService.updateCustomer(this.customer).subscribe({
      next: () => { this.success = true; },
      error: () => { this.failure = true; }
    });

  }


}

