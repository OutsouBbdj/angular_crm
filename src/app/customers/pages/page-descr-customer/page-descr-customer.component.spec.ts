import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageDescrCustomerComponent } from './page-descr-customer.component';

describe('PageDescrCustomerComponent', () => {
  let component: PageDescrCustomerComponent;
  let fixture: ComponentFixture<PageDescrCustomerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageDescrCustomerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageDescrCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
