import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageAddCustomerComponent } from './pages/page-add-customer/page-add-customer.component';
import { PageEditCustomerComponent } from './pages/page-edit-customer/page-edit-customer.component';
import { PageListCustomerComponent } from './pages/page-list-customer/page-list-customer.component';
import {PageDescrCustomerComponent} from "./pages/page-descr-customer/page-descr-customer.component";

const routes: Routes = [
  {path:'list', component: PageListCustomerComponent},
  {path:'add', component: PageAddCustomerComponent},
  {path:'description/:id', component: PageDescrCustomerComponent},
  {path:'edit/:id', component: PageEditCustomerComponent},
  {path:'', redirectTo: 'list', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomersRoutingModule { }
