import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Customer} from 'src/app/core/models/customer';
import {AuthService} from "../../auth/service/auth.service";
import {Injectable} from "@angular/core";


@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private url = '/api';

  constructor(private httpClient: HttpClient,
              private authService : AuthService) { }

  getAllCustomers(): Observable<Customer[]> {
    return this.httpClient.get<Customer[]>(`${this.url}/customers`,{headers: this.authService.getHeaders()});
  }

  getCustomerById(customerId: number): Observable<Customer> {
    return this.httpClient.get<Customer>(`${this.url}/customers/${customerId}`, {headers: this.authService.getHeaders()});
  }

  addCustomer(customer: Customer): Observable<void> {
    return this.httpClient.post<void>(`${this.url}/customers`, customer, {headers: this.authService.getHeaders()});
  }

  deleteCustomer(customerId: number): Observable<void> {
    return this.httpClient.delete<void>(`${this.url}/customers/${customerId}`, {headers: this.authService.getHeaders()});
  }

  updateCustomer(customer: Customer): Observable<void> {
    return this.httpClient.put<void>(`${this.url}/customers/${customer.id}`, customer, {headers: this.authService.getHeaders()});
  }
}
