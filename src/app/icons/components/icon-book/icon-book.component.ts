import { Component, OnInit } from '@angular/core';
import { faBookReader } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-icon-book',
  templateUrl: './icon-book.component.html',
  styleUrls: ['./icon-book.component.scss']
})
export class IconBookComponent implements OnInit {

  public icone = faBookReader;

  constructor() { }

  ngOnInit(): void {
  }

}
